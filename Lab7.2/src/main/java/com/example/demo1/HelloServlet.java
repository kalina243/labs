package com.example.demo1;

import java.io.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;

@WebServlet("/helloWorld")
//начальная страница приветствия
public class HelloServlet extends HttpServlet {
    private String message;
//Методы инициализации где мы присваиваем переменной message определенную строку
    public void init() {
        message = "Hello Servlet!";
    }
//метод отправки пользователю HTML страницы
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/html");

        PrintWriter out = response.getWriter();
        out.println("<html><body>");
        out.println("<h1>" + message + "</h1>");
        out.println("</body></html>");
    }
}