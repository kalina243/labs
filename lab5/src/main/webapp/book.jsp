<%@ page import="com.example.demo1.ConnectionManager" %>
<%@ page import="com.example.demo1.models.Book" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
 <title>Book store</title>
</head>
<body>
<section>
 <% ConnectionManager.getConnection(); %>
 <jsp:useBean id="bookDAO" scope="application"
class="com.example.demo1.dao.BooksDAOImpl"/>
 <% for (Book book : bookDAO.selectAllBooks()) {%>
 <p>book: <%= book.getName() %>, author <%= book.getAuthor() %> - pages <%=
book.getCountPages() %> |
 <a href="updateBook?action=update&id=<%= book.getId() %>">update</a>
 </p>
 <% } %>
 <a href="newbook.jsp">add new book</a>
</section>
</body>
</html>
