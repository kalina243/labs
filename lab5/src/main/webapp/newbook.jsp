<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
 <title>Add new book</title>
</head>
<body>
<section>
 <form method="post" action="addBook"/>
 <input type="hidden" name="id" value="0"/>
 <p>name: <input type="text" name="name" value=""/></p>
 <p>author: <input type="text" name="author" value=""/></p>
 <p>pages: <input type="number" name="pages" value=""/></p>
 <button type="submit">Save</button>
 </form>
</section>
</body>
</html>