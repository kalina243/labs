package com.example.demo1.dao;

import com.example.demo1.SecurityConfig;
import com.example.demo1.models.UserAccount;

import java.util.HashMap;
import java.util.Map;

/**
 * The type Data dao.
 * author Kalinkova Darya
 */
// класс взаимодействия с БД
public class DataDAO {
   // заглушка в виде map
   private static final Map<String, UserAccount> mapUsers = new HashMap<String, UserAccount>();

   // блок инициализации, в котором добавляем новых пользователей в БД
   {
      UserAccount emp = new UserAccount("employee1", "123", UserAccount.GENDER_MALE, //
            SecurityConfig.ROLE_EMPLOYEE);
      UserAccount mng = new UserAccount("man", "123", UserAccount.GENDER_MALE, //
            SecurityConfig.ROLE_EMPLOYEE, SecurityConfig.ROLE_MANAGER);
      mapUsers.put(emp.getUserName(), emp);
      mapUsers.put(mng.getUserName(), mng);
   }

   /**
    * Find user user account.
    *
    * @param userName the user name
    * @param password the password
    * @return the user account
    */
// проверка на валидность
   public static UserAccount findUser(String userName, String password) {
      UserAccount u = mapUsers.get(userName);
      if (u != null && u.getPassword().equals(password)) {
         return u;
      }
      return null;
   }

}