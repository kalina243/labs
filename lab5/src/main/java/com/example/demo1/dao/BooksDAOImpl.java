package com.example.demo1.dao;

import com.example.demo1.ConnectionManager;
import com.example.demo1.models.Book;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * The type Books dao.
 * author Kalinkova Darya
 */
public class BooksDAOImpl implements BooksDAO {
    private static final String SELECT_ALL = "SELECT id, name, author, count FROM book ORDER BY name ASC";
    private static final String SELECT_BOOK = "SELECT id, name, author, count FROM book WHERE id =? ORDER BY name ASC";
    private static final String UPDATE_BOOK = "UPDATE book SET name=?, author=?, count=? WHERE id =?";
    private static final String INSERT_BOOK = "INSERT INTO book (id, name, author, count) VALUES(?, ?, ?, ?)";
    /**
     * The Logger.
     */
    protected final Logger logger = LogManager.getLogger(BooksDAOImpl.class);

    @Override
    public Book selectBookById(Long idBook) {
        try (PreparedStatement ps =
                     ConnectionManager.getConnection().prepareStatement(SELECT_BOOK)) {
            ps.setLong(1, idBook);
            try (ResultSet rs = ps.executeQuery()) {
                rs.next();
                return fillBook(rs);
            }
        } catch (SQLException e) {
            logger.error("Error SQL select book");
        }
        return null;
    }

    @Override
    public List<Book> selectAllBooks() {
        List<Book> list = new ArrayList<>();
        try (PreparedStatement ps =
                     ConnectionManager.getConnection().prepareStatement(SELECT_ALL)) {
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    list.add(fillBook(rs));
                }
            }
        } catch (SQLException e) {
            logger.error("Error SQL select book");
        }
        return list;
    }

    @Override
    public void updateBook(Book book) {
        try (PreparedStatement ps =
                     ConnectionManager.getConnection().prepareStatement(UPDATE_BOOK)) {
            ps.setString(1, book.getName());
            ps.setString(2, book.getAuthor());
            ps.setInt(3, book.getCountPages());
            ps.setLong(4, book.getId());
            ps.executeUpdate();
        } catch (SQLException e) {
            logger.error("Error SQL update book");
        }
    }

    @Override
    public void addBook(Book book) {
        try (PreparedStatement ps =
                     ConnectionManager.getConnection().prepareStatement(INSERT_BOOK)) {
            ps.setLong(1, book.getId());
            ps.setString(2, book.getName());
            ps.setString(3, book.getAuthor());
            ps.setInt(4, book.getCountPages());
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Fill book book.
     *
     * @param rs the rs
     * @return the book
     * @throws SQLException the sql exception
     */
    public Book fillBook(ResultSet rs) throws SQLException {
        long id = rs.getLong("id");
        String name = rs.getString("name");
        String author = rs.getString("author");
        Integer countPages = rs.getInt("count");
        return new Book(id, name, author, countPages);
    }
}