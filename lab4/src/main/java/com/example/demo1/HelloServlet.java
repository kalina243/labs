package com.example.demo1;

import java.io.*;
import javax.servlet.http.*;

// класс приветсвия
public class HelloServlet extends HttpServlet {
    // поле, в котором хранится сообщение
    private String message;
    // инициализация переменной message
    public void init() {
        message = "Hello Servlet!";
    }

    // отправка пользователя html страницы
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/html");

        PrintWriter out = response.getWriter();
        out.println("<html><body>");
        out.println("<h1>" + message + "</h1>");
        out.println("</body></html>");
    }
}