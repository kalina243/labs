package com.example.demo1;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

// класс авторизации
public class Login extends HttpServlet {

    // получение данных от пользователя и занесение данных в переменные
    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        String userName = request.getParameter("userName");
        String password = request.getParameter("password");
        // проверка на корректность
        UserAccount userAccount = DataDAO.findUser(userName, password);

        // если пользователь ввел некорректные данные, то выводится html страница ошибки
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        if (userAccount == null) {
            String errorMessage = "Invalid userName or Password";
            out.println("<html><body>");
            out.println("<h1>" + errorMessage + "</h1>");
            out.println("</body></html>");
            return;
        }
        // если данные корректные, то вывдоится html страница с ОК
        out.println("<html><body>");
        out.println("<h1>" + "Ok" + "</h1>");
        out.println("</body></html>");
    }
}